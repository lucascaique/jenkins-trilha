FROM debian:latest
# define o mantenedor da imagem/coloca um metadado para o container
LABEL maintainer="marcos.araujo"
RUN apt-get update && apt-get clean
# instala o Nginx para testar
RUN apt-get install nginx -y
# estabelecendo caminho do meu ambiente para 0 container
COPY ./index.html /var/www/html/index.html
# expondo a porta para acesso
EXPOSE 80
# Comando para iniciar o NGINX no container
CMD ["nginx", "-g", "daemon off;"]
